package com.mcris.crudSupplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mcris.crudSupplier.entities.Supplier;
import com.mcris.crudSupplier.repositories.SupplierRepository;

@SpringBootApplication
public class CrudSupplierApplication implements CommandLineRunner{
	
	@Autowired
	private SupplierRepository supplierRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(CrudSupplierApplication.class, args);
	}

	public void run(String... arg0) throws Exception {

		supplierRepository.save(new Supplier("Icn Pharmaceuticals Inc", "Catalina", "Tillotson", "609373333", "Margate City", "3338 A Lockport Pl #6", "8402", "Margate City", "Atlantic"));
		supplierRepository.save(new Supplier("New England Sec Equip Co Inc", "Lawrence", "Lorens", "401465643", "Providence", "9 Hwy", "2906", "Providence", "Providence"));
		supplierRepository.save(new Supplier("Tippett, Troy M Ii", "Carlee", "Boulter", "785347180", "Abilene", "8284 Hart St", "67410", "Abilene", "Dickinson"));
		supplierRepository.save(new Supplier("Atc Contracting", "Thaddeus", "Ankeny", "916920357", "Roseville", "5 Washington St #1", "95678", "Roseville", "Placer"));
		supplierRepository.save(new Supplier("Pagano, Philip G Esq", "Jovita", "Oles", "386248411", "Daytona Beach", "8 S Haven St", "32114", "Daytona Beach", "Volusia"));
		supplierRepository.save(new Supplier("Kwikprint", "Alesia", "Hixenbaugh", "202646751", "Washington", "9 Front St", "20001", "Washington", "District of Columbia"));
		supplierRepository.save(new Supplier("Buergi & Madden Scale", "Lai", "Harabedian", "415423329", "Novato", "1933 Packer Ave #2", "94945", "Novato", "Marin"));
		supplierRepository.save(new Supplier("Inner Label", "Brittni", "Gillaspie", "208709123", "Boise", "67 Rv Cent", "83709", "Boise", "Ada"));
		supplierRepository.save(new Supplier("Hermar Inc", "Raylene", "Kampa", "574499145", "Elkhart", "2 Sw Nyberg Rd", "46514", "Elkhart", "Elkhart"));
		supplierRepository.save(new Supplier("Simonton Howe & Schneider Pc", "Flo", "Bookamer", "308726218", "Alliance", "89992 E 15th St", "69301", "Alliance", "Box Butte"));
		supplierRepository.save(new Supplier("Warehouse Office & Paper Prod", "Jani", "Biddy", "206711649", "Seattle", "61556 W 20th Ave", "98104", "Seattle", "King"));
		supplierRepository.save(new Supplier("Affiliated With Travelodge", "Chauncey", "Motley", "407413484", "Orlando", "63 E Aurora Dr", "32804", "Orlando", "Orange"));
		
		System.out.println(">>>>>>>>>>>>> Data Entry done!");
	}
}
