package com.mcris.crudSupplier.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mcris.crudSupplier.entities.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {
	
	public Optional<Supplier> findById(Integer id);

	public List<Supplier> findAllById(int id);

	public List<Supplier> findAllByCompanyNameContainingIgnoreCase(String companyName);

	public List<Supplier> findAllByFirstNameContainingIgnoreCase(String firstName);

	public List<Supplier> findAllByLastNameContainingIgnoreCase(String lastName);

	public List<Supplier> findAllByVatNumberContainingIgnoreCase(String vatNumber);

	public List<Supplier> findAllByIrsOfficeContainingIgnoreCase(String irsOffice);

	public List<Supplier> findAllByAddressContainingIgnoreCase(String address);

	public List<Supplier> findAllByZipCodeContainingIgnoreCase(String zipCode);

	public List<Supplier> findAllByCityContainingIgnoreCase(String city);

	public List<Supplier> findAllByCountryContainingIgnoreCase(String country);

	public List<Supplier> findAllByCompanyNameContainingIgnoreCaseAndFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndVatNumberContainingIgnoreCaseAndIrsOfficeContainingIgnoreCaseAndAddressContainingIgnoreCaseAndZipCodeContainingIgnoreCaseAndCityContainingIgnoreCaseAndCountryContainingIgnoreCase(
			String companyName, String firstName, String lastName, String vatNumber, String irsOffice, String address,
			String zipCode, String city, String country);
}
