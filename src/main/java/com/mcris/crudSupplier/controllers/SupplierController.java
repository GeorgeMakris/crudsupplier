package com.mcris.crudSupplier.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mcris.crudSupplier.entities.Supplier;
import com.mcris.crudSupplier.repositories.SupplierRepository;;

@Controller
public class SupplierController {

	@Autowired
	private SupplierRepository supplierRepository;

	private int itemsPerPage = 10;

	@GetMapping("/")
	public String goToList(Model model, @RequestParam(defaultValue = "0") int page) {
		return "redirect:/list";
	}
	
	@GetMapping("/list")
	public String showPage(Model model, @RequestParam(defaultValue = "0") int page) {
		Page<Supplier> pageSupplier = supplierRepository.findAll(new PageRequest(page, itemsPerPage));
		model.addAttribute("data", pageSupplier);
		model.addAttribute("currentPage", page);
		return "index";
	}

	@GetMapping("/search")
	public String searchPage(Model model, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "0") Integer id, @RequestParam(defaultValue = "") String companyName,
			@RequestParam(defaultValue = "") String firstName, @RequestParam(defaultValue = "") String lastName,
			@RequestParam(defaultValue = "") String vatNumber, @RequestParam(defaultValue = "") String irsOffice,
			@RequestParam(defaultValue = "") String address, @RequestParam(defaultValue = "") String zipCode,
			@RequestParam(defaultValue = "") String city, @RequestParam(defaultValue = "") String supplier) {
		List<Supplier> data = null;
		data = searchAll(id, companyName, firstName, lastName, vatNumber, irsOffice, address, zipCode, city, supplier);
		Page<Supplier> pageSupplier = new PageImpl<Supplier>(data, new PageRequest(page, itemsPerPage/* , sort */),
				data.size());
		model.addAttribute("data", pageSupplier);
		model.addAttribute("currentPage", page);
		return "index";
	}

	@GetMapping("/supplierDetails")
	public String showDetails(Model model, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "0") Integer id) {
		if(id<1) {
			return "redirect:/";
		}
		List<Supplier> data = supplierRepository.findAllById(id);
		Page<Supplier> pageSupplier = new PageImpl<Supplier>(data, new PageRequest(page, itemsPerPage/* , sort */),
				data.size());
		
		model.addAttribute("data", pageSupplier);
		model.addAttribute("currentPage", page);
		return "supplierDetails";
	}

	@PostMapping("/save")
	public String save(Integer id, String companyName, String vatNumber) {
		Supplier supplier = new Supplier(id, companyName, vatNumber); 
		supplierRepository.save(supplier);
		System.out.println("save: " + supplier.toString());
		return "redirect:/list";
	}

	@PostMapping("/saveDetails")
	public String saveDetails(Integer id, String companyName, String firstName, String lastName, String vatNumber,
			String irsOffice, String address, String zipCode, String city, String country) {
		Supplier supplier = new Supplier(id, companyName, firstName, lastName, vatNumber, irsOffice, address, zipCode, city, country); 
		supplierRepository.save(supplier);
		System.out.println("saveDetails: " + supplier.toString());
		return "redirect:/supplierDetails/?id="+id;
	}

	@GetMapping("/delete")
	public String delete(Integer id) {
		supplierRepository.deleteById(id);
		return "redirect:/list";
	}

	/* ~~~~~~~~~~~~~~~~~~~~~ ResponseBody ~~~~~~~~~~~~~~~~~~~~~ */

	@GetMapping("/findOne")
	@ResponseBody
	public Optional<Supplier> findOne(Integer id) {
		return supplierRepository.findById(id);
	}

	@GetMapping("/searchAll")
	@ResponseBody
	public List<Supplier> searchAll(Integer id, String companyName, String firstName, String lastName, String vatNumber,
			String irsOffice, String address, String zipCode, String city, String country) {
		if (id != null && id > 0) {
			return supplierRepository.findAllById(id);
		}
		return supplierRepository
				.findAllByCompanyNameContainingIgnoreCaseAndFirstNameContainingIgnoreCaseAndLastNameContainingIgnoreCaseAndVatNumberContainingIgnoreCaseAndIrsOfficeContainingIgnoreCaseAndAddressContainingIgnoreCaseAndZipCodeContainingIgnoreCaseAndCityContainingIgnoreCaseAndCountryContainingIgnoreCase(
						companyName, firstName, lastName, vatNumber, irsOffice, address, zipCode, city, country);

	}

}
