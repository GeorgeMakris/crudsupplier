package com.mcris.crudSupplier.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Supplier {

	@Id
	@GeneratedValue
	private Integer id;
	private String companyName;
	private String firstName;
	private String lastName;
	private String vatNumber;
	private String irsOffice;
	private String address;
	private String zipCode;
	private String city;
	private String country;
	
	public Supplier() {
		super();
	}
	
	public Supplier(Integer id) {
		super();
		this.id = id;
		this.companyName = "companyName" +id;
		this.firstName = "firstName" + id;
		this.lastName = "lastName" + id;
		this.vatNumber = "vatNumber" + id;
		this.irsOffice = "irsOffice" + id;
		this.address = "address" + id;
		this.zipCode = "zipCode" + id;
		this.city = "city" + id;
		this.country = "country" + id;
	}
	
	public Supplier(Integer id, String companyName, String vatNumber) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.firstName = "firstName" + id;
		this.lastName = "lastName" + id;
		this.vatNumber = vatNumber;
		this.irsOffice = "irsOffice" + id;
		this.address = "address" + id;
		this.zipCode = "zipCode" + id;
		this.city = "city" + id;
		this.country = "country" + id;
	}

	public Supplier(Integer id, String companyName, String firstName,
			String lastName, String vatNumber, String irsOffice,
			String address, String zipCode, String city, String country) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.vatNumber = vatNumber;
		this.irsOffice = irsOffice;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
	}

	public Supplier(String companyName, String firstName,
			String lastName, String vatNumber, String irsOffice,
			String address, String zipCode, String city, String country) {
		super();
		this.companyName = companyName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.vatNumber = vatNumber;
		this.irsOffice = irsOffice;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getIrsOffice() {
		return irsOffice;
	}

	public void setIrsOffice(String irsOffice) {
		this.irsOffice = irsOffice;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Supplier [id=" + this.id + ", companyName=" + this.companyName
				+ ", firstName=" + this.firstName + ", lastName=" + this.lastName
				+ ", vatNumber=" + this.vatNumber + ", irsOffice=" + this.irsOffice
				+ ", address=" + this.address + ", zipCode=" + this.zipCode + ", city="
				+ this.city + ", country=" + this.country + "]";
	}

	
	
}
