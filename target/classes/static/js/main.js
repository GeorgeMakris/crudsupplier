/**
 * 
 */

function isNumeric(str) {
	return /^[0-9]+$/.test(str);
}

function search() {
	var href="nothing"
	var id = $('#inputSearchId').val();
	var companyName = $('#inputSearchCompanyName').val();
	var vatNumber = $('#inputSearchVatNumber').val();
	if (!isNumeric(id)) { // The id is integer and >0 !
		id=0;
	}
	
	var redirect = "/search/?id=" + id + "&companyName=" + companyName + "&vatNumber="+vatNumber;
	location.href = redirect;
}

$(document).ready(function(){

	$('.container .nBtn').on('click', function(event) {
//		alert("New button clicked");
		var href = $(this).attr('href');
		var text = $(this).text();
		event.preventDefault();
		$('.myForm .id').val('');
		$('.myForm .companyName').val('');
		$('.myForm .vatNumber').val('');
		$('.myForm .saveModal').modal();
	});
	

	$('.table .delBtn').on('click', function(event) {
//		alert("Delete button clicked");
		var href = $(this).attr('href');
		event.preventDefault();
		$('.deleteModal .delRef').attr('href', href);
		$('.deleteModal').modal();

	});
	
	$('.searchBtn').on('click', function(event) {
		search();
	});
	
	$('.editBtn').on('click', function(event){
		var href = $(this).attr('value');
		href = "/findOne/?id="+href;
		var text = $(this).text();
		event.preventDefault();
		$.get(href, function(supplier, status) {
			$('.myForm .id').val(" " && supplier.id);
			$('.myForm .companyName').val(supplier.companyName);
			$('.myForm .vatNumber').val(supplier.vatNumber);
		});
		$('.myForm .saveModal').modal();
		return false; 
	});
	
	$('.searchText').keyup(function(e){
	    if(e.keyCode == 13){ // if Enter button pressed
	        search();
	    }
	});
});

