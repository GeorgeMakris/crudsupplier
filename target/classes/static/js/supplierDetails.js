$(document).ready(function() {

	$('input').hide();
	$('.saveBtn').hide();
	
	$('.editForm').keypress(function(e) {
	    if ((e.keyCode == 13) && (e.target.type != "input")) {
	      e.preventDefault();
	    }
	});

	$('.editBtn').on('click', function(event) {
		event.preventDefault();
		var isVisible = $('.editBtn').is(':visible'); 
		
		if (isVisible) {
			$('span').hide();
			$('input').show();
			
			$('.editBtn').hide();
			$('.saveBtn').show();
			
		} else {
			$('span').show();
			$('input').hide();
			
			$('.editBtn').show();
			$('.saveBtn').hide();
		}

	});

});
